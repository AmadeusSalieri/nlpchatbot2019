import pickle
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')

objects = None
with (open("../google.pickle", "rb")) as openfile:
    while True:
        try:
            objects = pickle.load(openfile)
        except EOFError:
            break

tokens = nltk.word_tokenize(objects[3])

print(tokens)

tagged = nltk.pos_tag(tokens)

print(tagged)
