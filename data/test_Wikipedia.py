#!/usr/bin/env python3

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import stopwords
import bs4 as bs
import urllib.request
import re

from colorama import Fore, Back, Style

import nltk
import numpy as np
import random
import string


class bcolors:
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class previous_word:
    is_answer = False,
    is_still_date = False,


def chose_bot():
    wiki_url = 'https://en.wikipedia.org/wiki/'
    raw_data = ""
    print("> ", end="")
    user_bot_search = input()
    user_bot_search = user_bot_search.replace(" ", "_")
    wiki_url += user_bot_search
    try:
        print("Ok, wait for a second, I'm going to call the "+user_bot_search+"_bot")
        raw_data = urllib.request.urlopen(wiki_url)
        return (raw_data, wiki_url)
    except:
        print("ERROR: try again...")
        return chose_bot()

print("Reception Bot: Hello, which bot are you going to consult today ?")
raw_data, wiki_url = chose_bot()

raw_data = raw_data.read()

html_data = bs.BeautifulSoup(raw_data, 'lxml')

all_paragraphs = html_data.find_all('p')

article_content = ""

for p in all_paragraphs:
    article_content += p.text

article_content = article_content.lower()
article_content = re.sub(r'\[[0-9]*\]', ' ', article_content)
article_content = re.sub(r'\s+', ' ', article_content)
stop = stopwords.words('english') + list(string.punctuation)
sentence_list = [i for i in nltk.sent_tokenize(article_content) if i not in stop]
article_words = [i for i in nltk.word_tokenize(article_content) if i not in stop]
print (article_words)
lemmatizer = nltk.stem.WordNetLemmatizer()


def LemmatizeWords(words):
    return [lemmatizer.lemmatize(word) for word in words]


remove_punctuation = dict((ord(punctuation), None)
                          for punctuation in string.punctuation)


def RemovePunctuations(text):
    return LemmatizeWords(nltk.word_tokenize(text.lower().translate(remove_punctuation)))


greeting_input_texts = ("hey", "heys", "hello",
                        "morning", "evening", "greetings",)
greeting_replie_texts = ["hey", "hey hows you?",
                         "*nods*", "hello there", "ello", "Welcome, how are you"]


def reply_greeting(text):
    for word in text.split():
        if word.lower() in greeting_input_texts:
            return random.choice(greeting_replie_texts)


def give_reply(user_input):
    chatbot_response = ''
    sentence_list.append(user_input)
    word_vectors = TfidfVectorizer(
        tokenizer=RemovePunctuations, stop_words='english')
    vecrorized_words = word_vectors.fit_transform(sentence_list)
    similarity_values = cosine_similarity(
        vecrorized_words[-1], vecrorized_words)
    similar_sentence_number = similarity_values.argsort()[0][-2]
    similar_vectors = similarity_values.flatten()
    similar_vectors.sort()
    matched_vector = similar_vectors[-2]
    if(matched_vector == 0):
        chatbot_response = chatbot_response+"I am sorry! I don't understand you"
        return chatbot_response
    else:
        chatbot_response = chatbot_response + \
            sentence_list[similar_sentence_number]

        return chatbot_response


def find_date(word_reply):
    days = ["monday", "tuesday", "wednesday",
            "thursday", "friday", "saturday", "sunday"]
    months = ["january", "february", "march", "april", "may", "june",
              "july", "august", "september", "october", "november", "december"]
    day_number = []
    for i in range(0, 31):
        day_number.append(str(i))
    year_re = "^[12][0-9]{3}"

    if word_reply in days or word_reply in months or word_reply in day_number or re.search(year_re, word_reply):
        previous_word.is_still_date = True
        return True
    previous_word.is_still_date = False
    return False


def what(word_reply, user_input):
    trigger_word = ["is", "was"]
    return False


def where(word_reply, user_input):
    trigger_word = ["in", "at"]
    if word_reply in trigger_word or previous_word.is_answer == True:
        print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
        if previous_word.is_answer == True:
            previous_word.is_answer = False
        else:
            previous_word.is_answer = True
        return True
    return False


def when(word_reply, user_input):
    trigger_word = ["in", "till", "of", "on", "from", "year", "to"]
    if word_reply in trigger_word or find_date(word_reply):
        print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
        return True
    return False


def who(word_reply, user_input):
    trigger_word = ["is", "was"]
    return False


def how(word_reply, user_input):
    print(Fore.MAGENTA + word_reply)
    return False


def why(word_reply, user_input):
    trigger_word = ["because", "to", "for"]
    if word_reply in trigger_word or previous_word.is_answer == True:
        print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
        return True
    return False


def specific_question(word_reply, user_input):
    for i in funct_dict:
        if user_input.split(' ', 1)[0] == i:
            return funct_dict[i](word_reply, user_input)


def bot_response(user_input):
    bot_reply = give_reply(user_input)
    for word_reply in bot_reply.split():
        in_input = False
        for word_input in user_input.split():
            if RemovePunctuations(word_input) == RemovePunctuations(word_reply) and word_input not in set(stopwords.words('english')):
                print(bcolors.UNDERLINE + word_reply + bcolors.ENDC, end="")
                in_input = True
        if not in_input:
            has_printed = False
            if user_input.split(' ', 1)[0] in funct_dict:
                has_printed = specific_question(word_reply, user_input)
            if not has_printed:
                print(word_reply, end="")
        print(" ", end="")
    print()


funct_dict = {
    "what": what,
    "where": where,
    "who": who,
    "when": when,
    "how": how,
    "why": why
}

continue_discussion = True
bot_name = wiki_url[wiki_url.find(
    "/wiki/") + 6:] + "_bot"
print("Hello, I am the", wiki_url[wiki_url.find(
    "/wiki/") + 6:] + "_bot, what do you want to know ?")
while(1):
    print("> ", end="")
    user_input = input()
    user_input = user_input .lower()
    if (user_input == "quit"):
        break
    print(bot_name + ": ", end="")
    bot_response(user_input)
    sentence_list.remove(user_input)
