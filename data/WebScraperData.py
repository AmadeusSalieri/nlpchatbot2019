"""
RADAT SAMUEL
19129053

"""

import pickle

import bs4
import requests
import sklearn

from utils import text_prepare


class WebScraperData:

    def __init__(self, url):

        self.pickle_file_name = 'rawdata.pickle'
        self.pickle_matrix_file_name = 'rawdata_matrix.pickle'

        # Retrieving data from website
        response = requests.get(url)
        if response.status_code == 404:
            raise Exception("an error occurred while requesting page %s" % url)

        self.soup = bs4.BeautifulSoup(response.text, "lxml")
        try:
            self.text = [p.text for p in self.soup.find(class_="mw-content-ltr").find_all("p")]
        except:
            raise Exception("unable to parse %s" % url)

    def get_sublinks(self):
        return self.soup.find_all('a', href=True)

    def save_to_disk(self):
        print(self.pickle_file_name)
        # Save retrieved data into local disk with pickle library
        with open(self.pickle_file_name, 'wb') as handle:
            pickle.dump(self.text, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def clean_text(self):
        self.text = [text_prepare(text) for text in self.text]

    def get_matrix(self):
        self.save_matrix()
        return self.matrix

    def save_matrix(self):
        with open(self.pickle_matrix_file_name, 'wb') as handle:
            pickle.dump(str(self.matrix), handle, protocol=pickle.HIGHEST_PROTOCOL)
