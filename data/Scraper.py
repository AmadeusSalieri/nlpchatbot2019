"""
RADAT SAMUEL
19129053

"""

import bs4
import requests
from src.WikipediaScraperData import WikipediaScraperData
from utils import text_prepare

WIKIPEDIA_PORTALS_URL = "https://en.wikipedia.org/wiki/Portal:Contents/Portals"


class Scraper:

    def __init__(self, start_urls: list):

        self.start_urls = start_urls
        self.data_list = dict()


class WikipediaScraper(Scraper):

    def __init__(self, start_urls: list):

        super().__init__(start_urls)
        self.all_portals = dict()

    def get_all_portals_links(self):
        response = requests.get(WIKIPEDIA_PORTALS_URL)
        if response.status_code == 404:
            raise Exception("an error occurred while requesting page")

        soup = bs4.BeautifulSoup(response.text, "lxml")
        portals_elements = [p for p in soup.find_all(class_="box-header-title-container flex-columns-noflex")]
        tmp = [{
            text_prepare(p.text).replace('see page types', '').replace('edit watch', '').strip(): p.nextSibling.find_all('a', href=True)
        } for p in portals_elements if text_prepare(p.text).find('see page types') != -1]

        for elem in tmp:
            self.all_portals = {**self.all_portals, **elem}

    def scrape_all_urls(self):
        for url in self.start_urls:
            wikipedia_scraper = WikipediaScraperData(url)
            self.data_list[text_prepare(url.split('/')[-1])] = wikipedia_scraper

    def scrape_portal(self, portal_title, portal_url):
        self.data_list[portal_title] = ScrapePortal(portal_url)

    def run(self):
        self.get_all_portals_links()

        for portal in self.all_portals:
            self.scrape_portal(portal, 'https://en.wikipedia.org%s' % self.all_portals[portal][0]["href"])


class ScrapePortal:

    def __init__(self, portal_url, depth=5):

        self.depth = depth
        self.depth_index = 0
        self.scrape([portal_url])

    def scrape(self, urls=[]):

        sublinks = list()
        scraper_elements = []
        for u in urls:
            try:
                scraper_element = WikipediaScraperData(u)
            except:
                continue
            scraper_elements.append(scraper_element)
            sublinks.extend(scraper_element.get_sublinks())

        sublinks = [s["href"] for s in sublinks if s.attrs["href"].find('/') != -1]

        def filter_sublinks(link):
            if link.find('http') == -1:
                return 'https://en.wikipedia.org%s' % link
            else:
                return link
        sublinks = list(map(filter_sublinks, sublinks))
        sublinks = [s for s in sublinks if s.find("File:") == -1]

        if self.depth_index < self.depth:
            self.depth_index += 1
            return self.scrape(sublinks)
        else:
            return self


if __name__ == '__main__':

    scraper = WikipediaScraper([
        "https://en.wikipedia.org/wiki/Philosophy"
    ])

    scraper.run()
    print("ok")
