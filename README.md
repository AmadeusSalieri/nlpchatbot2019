# NLPChatbot2019

Chatbot using Natural Language Processing written in Python using NLTK

**Works with python 3.5>**

## Prepare and run

`pip install -r requirements.txt`
`cd src`
`python Main.py`