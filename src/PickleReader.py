import pickle
import nltk

from utils import text_prepare

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')


def pickle_file_opener(file_path):

    objects = None
    with (open(file_path, "rb")) as openfile:
        while True:
            try:
                objects = pickle.load(openfile)
            except EOFError:
                break

    return [text_prepare(elem) for elem in objects]


if __name__ == '__main__':

    tokens = nltk.word_tokenize(pickle_file_opener("../data/google.pickle")[3])

    print(tokens)

    tagged = nltk.pos_tag(tokens)

    print(tagged)
