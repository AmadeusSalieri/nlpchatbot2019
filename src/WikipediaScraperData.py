"""
RADAT SAMUEL
19129053

"""

import sklearn

from WebScraperData import WebScraperData
from utils import text_prepare


class WikipediaScraperData(WebScraperData):

    def __init__(self, url):

        super().__init__(url)
        self.pickle_file_name = '%s.pickle' % text_prepare(url.split('/')[-1])
        self.pickle_matrix_file_name = '%s_matrix.pickle' % text_prepare(url.split('/')[-1])

        try:
            self.wiki_table = self.soup.find('table', {'class': 'wikitable sortable'})
            if self.wiki_table is not None:
                print(str(self.wiki_table))
        except:
            pass

        self.save_to_disk()
        self.clean_text()
        self.matrix = sklearn.feature_extraction.text.CountVectorizer(self.text)
