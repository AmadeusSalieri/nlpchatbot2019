"""
RADAT SAMUEL
19129053

"""

import re
import nltk

from nltk.corpus import stopwords

nltk.download('stopwords')

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(stopwords.words('english'))


def text_prepare(text):
    """
        text: a string

        return: modified initial string
    """
    text = text.lower()  # lowercase text

    tokenizer = nltk.tokenize.WhitespaceTokenizer()
    token = tokenizer.tokenize(text)

    # replace REPLACE_BY_SPACE_RE symbols by space in text
    ttoken = token
    token = list()
    for t in ttoken:
        t = re.sub(REPLACE_BY_SPACE_RE, ' ', t)
        token.append(t)

    # delete symbols which are in BAD_SYMBOLS_RE from text
    ttoken = token
    token = list()
    for t in ttoken:
        t = re.sub(BAD_SYMBOLS_RE, '', t)
        if not t == '':
            token.append(t)

    # delete stopwords from text
    ttoken = token
    token = list()
    for t in ttoken:
        if t not in STOPWORDS:
            token.append(t)

    return ' '.join(token)
