import random
from typing import List, Any

import nltk

import mlconjug

conjugator = mlconjug.Conjugator(language='en')


class Idea:

    def __init__(self, tense='present', type='indicative', predicates=[], verb='', objects=[]):

        self.tense = tense
        self.type = type

        self.predicates = predicates
        self.verb = verb
        self.objects = objects


def conjugate_verb(verb, predicates, tense='indicative present'):

    PRP_LIST = ['I', 'You', 'He', 'We' 'You', 'They']

    person_number = '1'
    plurial = 's'

    test_verb = conjugator.conjugate(verb)
    all_conjugated_forms = {}
    for element in test_verb.iterate():
        if element[1] in all_conjugated_forms.keys():
            all_conjugated_forms[element[1]][element[2]] = element[3]
        else:
            all_conjugated_forms[element[1]] = {
                element[2]: element[3]
            }

    tagged = nltk.pos_tag(predicates)

    if len(predicates) > 1:
        plurial = 'p'
        person_number = '3'
    else:
        if tagged[-1][1] == 'PRP':
            person_number = str(PRP_LIST.index(tagged[-1][0]) + 1)
        else:
            if tagged[-1][1] == 'NNS':
                plurial = 'p'
            person_number = '3'

    conjugated_verb = all_conjugated_forms[tense]['%s%s' % (person_number, plurial)]

    return conjugated_verb


class SentenceGenerator:

    ideas: List[Idea]

    def __init__(self, ideas=[]):

        self.connectors = ['and', ',', '.']
        self.accumulators = ['and', ',', '+', 'and also', 'as well as']
        self.ending_punctuation = ['.', '?', '!']

        self.ideas = ideas

        self.sentence = str()
        self.sentence_tokens = list()
        self.compute_phrase()

    def get_phrase(self):
        return self.sentence

    def clean_sentence(self):
        tokens = nltk.tokenize.word_tokenize(self.sentence)
        last_element = list(nltk.pos_tag(tokens)[-1])

        if len(last_element.pop()) == 1 and last_element.pop() not in self.ending_punctuation:
            tokens.pop()

        self.construct_sentence(tokens)

    def compute_phrase(self):
        for idea in self.ideas:
            for predicate in idea.predicates:
                self.sentence_tokens.append(predicate)

            conjugated_verb = conjugate_verb(idea.verb, idea.predicates)

            self.sentence_tokens.append(conjugated_verb)

            for obj in idea.objects:
                self.sentence_tokens.append(obj)

            self.sentence_tokens.append(random.choice(self.connectors))

        self.construct_sentence()
        self.clean_sentence()

    def construct_sentence(self, tokens=None):
        if tokens is None:
            tokens = self.sentence_tokens
        self.sentence = nltk.tokenize.treebank.TreebankWordDetokenizer().detokenize(tokens)


if __name__ == '__main__':

    ideas = [
        Idea(predicates=['I'], verb='be', objects=['French']),
        Idea(predicates=['Ms. Sun'], verb='be', objects=['Chinese']),
        Idea(predicates=['Sardoche'], verb='be', objects=['fool']),
        Idea(predicates=['Asians'], verb='be', objects=['located', 'asia']),
    ]

    print(SentenceGenerator(ideas).get_phrase())
