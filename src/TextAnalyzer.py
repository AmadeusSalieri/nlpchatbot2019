"""
RADAT SAMUEL
19129053

"""

import nltk

from PickleReader import pickle_file_opener


class TextAnalyzer:

    def __init__(self, element, subject):

        self.subject = subject
        self.sentences = list()
        if type(element) == list:
            for e in element:
                self.sentences.extend(nltk.tokenize.sent_tokenize(e))
        else:
            self.sentences = nltk.tokenize.sent_tokenize(element)

        self.sentenceObjs = [Sentence(s) for s in self.sentences]


class Sentence:

    def __init__(self, sentence):
        self.sentence = sentence
        self.words = nltk.word_tokenize(sentence)
        self.sentence_tagging = nltk.pos_tag(self.words)
        self.wordObjs = [Word(w) for w in self.words]


class Word:

    def __init__(self, word):
        self.word = word
        self.letters = list(word)


if __name__ == '__main__':
    test = TextAnalyzer(pickle_file_opener("../data/google.pickle"), "Google")
    print(test.sentences)
