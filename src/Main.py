#!/usr/bin/env python3

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import stopwords
import bs4 as bs
import urllib.request
import re

from colorama import Fore, Back, Style

import nltk
import numpy as np
import random
import string
from os import walk, path

from TextAnalyzer import TextAnalyzer
from PickleReader import pickle_file_opener

nltk.download('punkt')
nltk.download('wordnet')


class bcolors:
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class previous_word:
    is_answer = False,
    is_still_date = False,


def prepare_text(article_content):
    article_content = article_content.lower()
    article_content = re.sub(r'/<[^>]*>/g', '', article_content)
    article_content = re.sub(r'\[[0-9]*\]', ' ', article_content)
    article_content = re.sub(r'\s+', ' ', article_content)
    return (article_content)


def LemmatizeWords(words):
    lemmatizer = nltk.stem.WordNetLemmatizer()
    return ' '.join(str(x) for x in [lemmatizer.lemmatize(word) for word in words])


class ChatBot:

    def __init__(self):

        self.funct_dict = {
            "what": self.what,
            "where": self.where,
            "who": self.who,
            "when": self.when,
            "how": self.how,
            "why": self.why
        }
        self.sentence_list = list()

        self.continue_discussion = True

        self.wiki_url = 'https://en.wikipedia.org/wiki/'

        self.retrieve_all_pickles()

        print("Initializing...")

        article_content = ' '.join(self.sentence_list)

        article_content = prepare_text(article_content)
        stop = stopwords.words('english') + list(string.punctuation)

        self.sentence_list.extend([i for i in nltk.sent_tokenize(
            article_content) if i not in stop])
        self.article_words = [i for i in nltk.word_tokenize(
            LemmatizeWords(article_content)) if i not in stop]

    def retrieve_all_pickles(self):
        f = []
        for (_, _, filenames) in walk('../data'):
            f.extend(filenames)

        print(path.join('../data', f[1]))
        test = [pickle_file_opener(path.join('../data', elem)) for elem in f if elem.find('.pickle') != -1]
        for s in test:
            self.sentence_list.extend(s)

    def run(self):

        print("bot: Hello, what do you want to know ?")
        while 1:
            print("> ", end="")
            user_input = input()
            user_input = prepare_text(user_input)
            if user_input == "quit":
                break
            print("bot: ", end="")
            self.bot_response(user_input)
            self.sentence_list.remove(user_input)

    @staticmethod
    def give_reply(user_input):
        chatbot_response = ''
        chatbot.sentence_list.append(user_input)
        word_vectors = TfidfVectorizer(max_df=0.8, ngram_range=(
            1, 2), token_pattern='(\S+)', stop_words='english')
        vectorized_words = word_vectors.fit_transform(chatbot.sentence_list)
        similarity_values = cosine_similarity(
            vectorized_words[-1], vectorized_words)
        similar_sentence_number = similarity_values.argsort()[0][-2]
        similar_vectors = similarity_values.flatten()
        similar_vectors.sort()
        matched_vector = similar_vectors[-2]
        if matched_vector == 0:
            chatbot_response = chatbot_response + "I am sorry! I don't understand you"
            return chatbot_response
        else:
            chatbot_response = chatbot_response + \
                               chatbot.sentence_list[similar_sentence_number]

            return chatbot_response

    @staticmethod
    def find_date(word_reply):
        days = ["monday", "tuesday", "wednesday",
                "thursday", "friday", "saturday", "sunday"]
        months = ["january", "february", "march", "april", "may", "june",
                  "july", "august", "september", "october", "november", "december"]
        day_number = []
        for i in range(0, 31):
            day_number.append(str(i))
        year_re = "^[12][0-9]{3}"

        if word_reply in days or word_reply in months or word_reply in day_number or re.search(year_re, word_reply):
            previous_word.is_still_date = True
            return True
        previous_word.is_still_date = False
        return False

    @staticmethod
    def which(word_reply, user_input):
        trigger_word = ["the", "this", "that"]
        if word_reply in trigger_word:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    @staticmethod
    def what(word_reply, user_input):
        trigger_word = ["is", "was"]
        if word_reply in trigger_word:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    @staticmethod
    def where(word_reply, user_input):
        trigger_word = ["in", "at"]
        if word_reply in trigger_word or previous_word.is_answer == True:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            if previous_word.is_answer == True:
                previous_word.is_answer = False
            else:
                previous_word.is_answer = True
            return True
        return False

    def when(self, word_reply, user_input):
        trigger_word = ["in", "till", "of", "on", "from", "year", "to"]
        if self.find_date(word_reply):
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    @staticmethod
    def who(word_reply, user_input):
        trigger_word = ["it's", "is", "was", "of"]
        if word_reply in trigger_word:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    @staticmethod
    def how(word_reply, user_input):
        trigger_word = ["by", "with"]
        if word_reply in trigger_word:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    @staticmethod
    def why(word_reply, user_input):
        trigger_word = ["because", "to", "for"]
        if word_reply in trigger_word:
            print(bcolors.BOLD + word_reply + bcolors.ENDC, end="")
            return True
        return False

    def specific_question(self, word_reply, user_input):
        for i in self.funct_dict:
            if user_input.split(' ', 1)[0] == i:
                return self.funct_dict[i](word_reply, user_input)

    def bot_response(self, user_input):
        bot_reply = self.give_reply(user_input)
        for word_reply in bot_reply.split():
            in_input = False
            for word_input in user_input.split():
                if prepare_text(word_input) == prepare_text(word_reply) and word_input not in set(
                        stopwords.words('english')):
                    print(bcolors.UNDERLINE + word_reply + bcolors.ENDC, end="")
                    in_input = True
            if not in_input:
                has_printed = False
                if user_input.split(' ', 1)[0] in self.funct_dict:
                    has_printed = self.specific_question(word_reply, user_input)
                if not has_printed:
                    print(word_reply, end="")
            print(" ", end="")
        print()


chatbot = ChatBot()
chatbot.run()
